# BoilerKey-rs

The date is Tuesday, October 30th, 2018. For months now ITAP has been warning us that we'd be forced to use a 2FA solution they've titled '*BoilerKey*', except of course because we were all lazy students, none of us did anything or learned how to use it and it was a complete shitshow. Who would have guessed?

A week later some brave soul decided to toy around with it on a technical level and gave us [local-boilerkey](https://github.com/elnardu/local-boilerkey). One day while not paying attention in our networking class, my friend and I realized we could totally throw boilerkey into an authenticator and bypass using duo. 

Then everyone and their dog made something for boilerkey, be it basically this but in go, or a browser extension. Naturally I had to get in the game.

Basically this is just a alternative authenticator so I don't have to pull my phone out at the moment.

# Roadmap
 - [ ] User friendly setup utility
 - [ ] Secure storage of secrets
 - [ ] Clipboard support (None of the clipboard libraries work properly on linux)
 - [ ] A system tray applet that you can request codes from
 - [ ] Browser extension in rust generated webassembly

# Setup
## Linux

Run the following commands

    mkdir ~/.local/share/boilerkeyauthenticator/
    touch ~/.local/share/boilerkeyauthenticator/secrets

Now we need to get your hotp secret so we can generate codes for you. Either get it from [local-boilerkey](https://github.com/elnardu/local-boilerkey) or the qr code on the boiler key setup site.

Next edit ~/.local/share/boilerkeyauthenticator/secrets and add the following data like so:

    totp token
    pin (optional, leave a blank line)
    0

After that a cargo build --release

## Windows & MacOS
In theory this should work fine, I used a cross platform library to set the config directories, but I'm too lazy while this is in an early alpha to figure out where you need to make folders when it will be automated eventually.