use directories::ProjectDirs;
use otpauth::HOTP;

use std::error::Error;
use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::path::Path;

struct Data {
    key: String,
    pin: String,
    count: u64,
}

fn read_hotp_data(hotp_config: &Path) -> Result<Data, Box<dyn Error>> {
    let file = File::open(hotp_config)?;
    let mut reader = BufReader::new(file);
    let mut file_content = String::new();
    reader.read_to_string(&mut file_content)?;
    let mut lines = file_content.lines();
    Ok(Data {
        key: lines.next().unwrap().to_string(),
        pin: lines.next().unwrap().to_string(),
        count: lines.next().unwrap().parse::<u64>()?,
    })
}

fn write_hotp_data(hotp_config: &Path, data: &Data) -> Result<(), Box<dyn Error>> {
    let mut file = File::create(hotp_config)?;
    writeln!(file, "{}", data.key)?;
    writeln!(file, "{}", data.pin)?;
    writeln!(file, "{}", data.count)?;
    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let hotp_config = {
        let data_dir = ProjectDirs::from("com", "mborder", "Boilerkey Authenticator").unwrap();
        let data_dir = data_dir.data_dir();
        data_dir.join("secrets")
    };
    let mut data = read_hotp_data(&hotp_config)?;
    let auth = HOTP::new(&data.key);
    let code = format!("{:06}", auth.generate(data.count)); //otpauth outputs a u32, this casues problems for codes with leading zeros
    println!("{},{}", &data.pin, &code);
    data.count += 1;
    write_hotp_data(&hotp_config, &data)?;
    Ok(())
}
